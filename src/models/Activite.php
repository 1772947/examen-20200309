<?php


class Activite
{
    private string $lieu;
    private string $date;
    private string $partenaires;
    private string $activite;
    private string $determinants;
    private string $intensite;
    private string $duree;
    private array $effet;
    private string $motivation;
    private string $plaisir;

    public function __construct($activiteDetails)
    {
        foreach ($activiteDetails as $detail => $valeur) {
            if (property_exists($this, $detail)) {
                $this->$detail = $valeur;
            }
        }
    }

    /**
     * @return string
     */
    public function getLieu(): string
    {
        return $this->lieu;
    }

    /**
     * @param string $lieu
     */
    public function setLieu(string $lieu): void
    {
        $this->lieu = $lieu;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getPartenaires(): string
    {
        return $this->partenaires;
    }

    /**
     * @param string $partenaires
     */
    public function setPartenaires(string $partenaires): void
    {
        $this->partenaires = $partenaires;
    }

    /**
     * @return string
     */
    public function getActivite(): string
    {
        return $this->activite;
    }

    /**
     * @param string $activite
     */
    public function setActivite(string $activite): void
    {
        $this->activite = $activite;
    }

    /**
     * @return string
     */
    public function getDeterminants(): string
    {
        return $this->determinants;
    }

    /**
     * @param string $determinants
     */
    public function setDeterminants(string $determinants): void
    {
        $this->determinants = $determinants;
    }

    /**
     * @return string
     */
    public function getIntensite(): string
    {
        return $this->intensite;
    }

    /**
     * @param string $intensite
     */
    public function setIntensite(string $intensite): void
    {
        $this->intensite = $intensite;
    }

    /**
     * @return string
     */
    public function getDuree(): string
    {
        return $this->duree;
    }

    /**
     * @param string $duree
     */
    public function setDuree(string $duree): void
    {
        $this->duree = $duree;
    }

    /**
     * @return array
     */
    public function getEffet(): array
    {
        return $this->effet;
    }

    /**
     * @param array $effet
     */
    public function setEffet(array $effet): void
    {
        $this->effet = $effet;
    }

    /**
     * @return string
     */
    public function getMotivation(): string
    {
        return $this->motivation;
    }

    /**
     * @param string $motivation
     */
    public function setMotivation(string $motivation): void
    {
        $this->motivation = $motivation;
    }

    /**
     * @return string
     */
    public function getPlaisir(): string
    {
        return $this->plaisir;
    }

    /**
     * @param string $plaisir
     */
    public function setPlaisir(string $plaisir): void
    {
        $this->plaisir = $plaisir;
    }
}